<?php

/**
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */

require_once 'ElFinderAssetsManager.php';

class TinyMceElFinder extends TinyMceFileManager
{
    public $settings = array();
    public $connectorRoute = 'admin/elfinder/connector2'; //false

    private $_id;
    private static $_counter=0;    
    
    protected $assetManager ; 
    
    public function init()
    {

        $this->assetManager = new ElFinderAssetsManager();
        $this->assetManager->registerAll();  
       
        // set required options
        if (empty($this->connectorRoute))
            throw new CException('$connectorRoute must be set!');
        
        $this->settings['url'] = Yii::app()->createUrl($this->connectorRoute);
        $this->settings['lang'] = Yii::app()->language;
        
        if (Yii::app()->getRequest()->enableCsrfValidation) {
            $this->settings['customData'] = array(Yii::app()->request->csrfTokenName=>Yii::app()->request->csrfToken);
        }
    } 
    
    public function getId()
    {
        if ($this->_id !== null)
            return $this->_id;
        else
            return $this->_id = 'elfd' . self::$_counter++;
    }

    public function getFileBrowserCallback()
    {
        $connectorUrl = $this->settings['url'];
        $id = $this->getId();
        $settings = CJavaScript::encode($this->settings);
        
        $dialogSettings =  CJavaScript::encode(array(
            'zIndex' => 400001,
            'width' => 900,
            'modal' => true,
            'title' => "Files",
        ));
        $this->assetManager->clientScript->registerScript('elFinder', "");


        $settings = $this->settings;


        $settings['getFileCallback'] = 'js:function(URL) {
                        console.log(aWin.document.forms[0].elements);
                        console.log(aFieldName);
                        console.log(URL);
                        $("#"+aFieldName).val(URL.url);
                        if (type == "image" && aFieldName=="src" && aWin.ImageDialog.showPreviewImage)
                            aWin.ImageDialog.showPreviewImage(URL.url);
                        $(\'#'.$id.'\').dialog("close");
                    }';

//        $settings['closeOnEditorCallback'] = true;

        $settings= CJavaScript::encode($settings);
        $script = <<<EOD
        function(field_name, url, type, win) {
            var aFieldName = field_name, aWin = win;
            if($("#$id").length == 0) {
                $("body").append($("<div/>").attr("id", "$id"));
                $('#$id').elfinder($settings).dialog($dialogSettings);
            }
            else {
                $('#$id').dialog('open');
            }
        }
EOD;

        return 'js:' . $script;
    }
}
