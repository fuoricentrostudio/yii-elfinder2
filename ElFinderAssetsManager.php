<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @property CAssetManager $assetsManager
 */
class ElFinderAssetsManager extends CComponent {
    
    public $root;
    public $clientScript;

    public function __construct($root=__DIR__) {
        
        $this->root = $root;
        $this->clientScript = Yii::app()->getClientScript();        
        
    }
            
    public function publish($path, $includeOnly=null, $excludeOnly=null){

        $originalExclude = $exclude = Yii::app()->getAssetManager()->excludeFiles;
        
        $fullPath = $this->root.DIRECTORY_SEPARATOR.$path;
        
        if(null !== $includeOnly ){    
            $exclude = array_merge($exclude, array_diff(scandir($fullPath), array_merge(array('..', '.'),  $includeOnly)));
        }
        
        if(null !== $excludeOnly ){    
            $exclude = array_merge($exclude, $excludeOnly);
        }        
        
        Yii::app()->getAssetManager()->excludeFiles = $exclude;
        
        $assetsUrl = Yii::app()->getAssetManager()->publish($fullPath);
        
        Yii::app()->getAssetManager()->excludeFiles = $originalExclude;
        
        return $assetsUrl;
        
    }
    
    public function registerAll($libPath='elFinder', $include=array('css','img','js')){
        
        $assetsUrl  = $this->publish($libPath, $include);
        
        $this->registerCss($assetsUrl);
        $this->registerJs($assetsUrl);
    }
    
    public function registerCss($assetsUrl){
                
        // jQuery and jQuery UI
        $this->clientScript->registerCssFile($this->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
        
        // elFinder CSS
        $this->clientScript->registerCssFile($assetsUrl . '/css/elfinder.min.css');
        $this->clientScript->registerCssFile($assetsUrl . '/css/theme.css');    
        
    }  
    
    public function registerJs($assetsUrl){
                
        // jQuery and jQuery UI
        $this->clientScript->registerCoreScript('jquery');
        $this->clientScript->registerCoreScript('jquery.ui');      
        
        // elFinder JS
        $this->clientScript->registerScriptFile($assetsUrl . '/js/elfinder.full.js');
        // elFinder translation
        $this->clientScript->registerScriptFile($assetsUrl . '/js/i18n/elfinder.it.js');        
        
    }      
    
}
