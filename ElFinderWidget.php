<?php
/**
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */

require_once 'ElFinderAssetsManager.php';

class ElFinderWidget extends CWidget
{
    /**
     * Client settings.
     * More about this: https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
     * @var array
     */
    public $settings = array();
    public $connectorRoute = 'admin/elfinder/connector2';//false
    protected $assetManager ; 

    public function init()
    {
        
        $this->assetManager = new ElFinderAssetsManager();
        $this->assetManager->registerAll();
        
        // set required options
        if(empty($this->connectorRoute))
            throw new CException('$connectorRoute must be set!');
        
        $this->settings['url'] = Yii::app()->createUrl($this->connectorRoute);
        $this->settings['lang'] = Yii::app()->language;
        
        if (Yii::app()->getRequest()->enableCsrfValidation) {
            $this->settings['customData'] = array(Yii::app()->request->csrfTokenName=>Yii::app()->request->csrfToken);
        }
        
    }

    public function run()
    {
        $id = $this->getId();
        $settings = CJavaScript::encode($this->settings);
        $this->assetManager->clientScript->registerScript('elFinder', "$('#$id').elfinder($settings);");
        echo "<div id=\"$id\"></div>";
    }

}
